#!/usr/bin/env python

"""
Ejercicio 1.

Se requiere determinar cuál de tres cantidades
proporcionadas es la mayor. El usuario debe proveer los
tres valores por entrada.
"""


numero1 = int(input("ingrese un número: "))
numero2 = int(input("ingrese un número: "))
numero3 = int(input("ingrese un número: "))

if numero1 > numero2:
    if numero1 > numero3:
        mayor = numero1
    else:
        mayor = numero3
else:
    if numero2 > numero3:
        mayor = numero2
    else:
        mayor = numero3

print("El número mayor es: " , mayor)
