#!/usr/bin/env python

"""
Ejercicio 3.

Una persona se encuentra en el kilómetro 190 de la carretera 5 sur,
otra se encuentra en el km 250 de la misma carretera, la primera
viaja en dirección norte, mientras que la segunda se dirige hacia el sur,
siempre a la misma velocidad. Piense la solución utilizando
el ciclo WHILE y FOR.
"""

norte = 190
sur = 251
velocidad = 1

# while
while norte < sur:
    norte = norte + velocidad
    sur = sur - velocidad
    # progreso
    print("Norte avanza", norte)
    print("Sur avanza", sur)

print("Norte y Sur se encuentran en el KM.", norte - 0.5)
