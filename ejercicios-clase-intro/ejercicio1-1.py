#!/usr/bin/env python

"""
Ejercicio 1.

Se requiere determinar cuál de tres cantidades proporcionadas
es la mayor. El usuario debe proveer los
tres valores por entrada.
"""

numero1 = input("Ingrese un número: ")
numero2 = input("Ingrese un número: ")
numero3 = input("Ingrese un número: ")

mayor = 0
if (numero1 > numero3) and (numero1 > numero2):
    mayor = numero1
elif (numero2 > numero1) and (numero2 > numero3):
    mayor = numero2
# elif (numero3 > numero1) and (numero3 > numero2):
else:
    mayor = numero3

print("El número mayor ", mayor)
